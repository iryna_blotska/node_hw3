const {Load} = require('../models/loadModel');
const {User} = require('../models/userModel');

const addLoadforUser = async (created_by, loadData) => {
    
    const user = await User.find({_id: created_by});
    if(!user[0].role === 'SHIPPER'){
        throw new Error('You don\'t have permisson to create loads');
    }
    const load = new Load({...loadData, created_by});
    await load.save();
}

const getUsersLoads = async (created_by, offset, limit) => {
    const user = await User.find({_id: created_by}).skip(offset).limit(limit);
    if(user[0].role === 'DRIVER'){
        const loads = await Load.find({assigned_to: created_by});
        return loads;    
    } else {
        const loads = await Load.find({created_by}).skip(offset).limit(limit);
        return loads;
    }
    
}
const getLoadByIdForUser =  async (loadId, created_by) => {
    const load = await Load.findOne({_id: loadId, created_by});
    return load;
}

const postLoadByIdForUser =  async (loadId, created_by) => {
    const load = await Load.findOne({_id: loadId, created_by});
    if(!load){
        throw new Error('Load not found');
    }
    await Load.findOneAndUpdate({_id: loadId}, { $set: {status: 'POSTED'}});
    return load;
}
const getUserActiveLoad = async(assigned_to) => {
    const load = await Load.findOne({assigned_to: assigned_to});
    if(!load){
        throw new Error('Load not found');
    }
    return load;
}
const iterateLoadState = async(assigned_to, state) => {
    await Load.findOneAndUpdate({_id: assigned_to}, {state: state});
}

module.exports = {
    addLoadforUser,
    getUsersLoads,
    getLoadByIdForUser,
    postLoadByIdForUser,
    getUserActiveLoad,
    iterateLoadState
};
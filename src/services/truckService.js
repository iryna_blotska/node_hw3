const {Truck} = require('../models/truckModel');
const {User} = require('../models/userModel');

const addTruckforUser = async (created_by, truckData) => {
    
    const user = await User.find({_id: created_by});
    if(!user[0].role === 'DRIVER'){
        throw new Error('You don\'t have permisson to create truks');
    }
    const truck = new Truck({...truckData, created_by});
    await truck.save();
}

const getUsersTrucks = async (created_by) => {
    const user = await User.find({_id: created_by});
    if(!user[0].role === 'DRIVER'){
        throw new Error('You don\'t have permisson to get truk information');
    }
    const trucks = await Truck.find({created_by}); 
    return trucks;
    
}
const getTruckByIdForUser =  async (truckId, created_by) => {
    const truck = await Truck.findOne({_id: truckId, created_by});
    return truck;
}

const updateTruckByIdForUser = async (truckId, created_by, data) => {
    const truckTypes = ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'];
    if(!truckTypes.includes(data.type)){
        throw new Error('Wrong truck type.');
    }
    await Truck.findOneAndUpdate({_id: truckId, created_by}, { $set: data});
}
const deleteTruckByIdForUser = async(created_by, truckId) => {
    const truck = await Truck.find({_id: truckId});
    if(!truck){
        throw new Error('Truck not found');
    }
    await Truck.findOneAndRemove({_id: truckId});

}
const assignTruckToUser = async(assigned_to, truckId) => {
    
    const truck = await Truck.findOne({_id: truckId});
    console.log(truck, assigned_to, truckId);
    if(!truck){
        throw new Error('Truck not found');
    }
    await Truck.findOneAndUpdate({_id: truckId}, {assigned_to: assigned_to, status: 'IS'});
}

const postLoadByIdForUser =  async (loadId, created_by) => {
    const load = await Load.findOne({_id: loadId, created_by});
    if(!load){
        throw new Error('Load not found');
    }
    await Load.findOneAndUpdate({_id: loadId}, { $set: {status: 'POSTED'}});
    return load;
}


module.exports = {
    addTruckforUser,
    getUsersTrucks,
    getTruckByIdForUser,
    updateTruckByIdForUser,
    deleteTruckByIdForUser,
    assignTruckToUser
};
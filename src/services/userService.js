const bcrypt = require('bcrypt');
const { User } = require('../models/userModel');

const getUserProfile = async(userId) => {
    const user = await User.find({_id: userId});
    console.log(user);
    return user;
}

const changeUserPassword = async({userId, oldPassword, newPassword}) => {
    const user = await User.find({_id: userId});
    if(!user){
        throw new Error('User not found');
    }
    const match = await bcrypt.compare(oldPassword, user[0].password);
    if(!match){
        throw new Error('Invalid password')
    }
    await User.findOneAndUpdate({_id: userId}, { $set: {password: await bcrypt.hash(newPassword, 10)}});
}
const deleteUserProfile = async(userId) => {
    const user = await User.find({_id: userId});
    if(!user){
        throw new Error('User not found');
    }
    if(!user[0].role === 'SHIPPER'){
        throw new Error('You don\'t have pernisson to delete account');
    }
    await User.findOneAndRemove({_id: userId});

}

module.exports = {
    getUserProfile,
    changeUserPassword,
    deleteUserProfile
}
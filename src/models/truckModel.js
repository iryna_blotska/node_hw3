const mongoose = require('mongoose');

const Truck = mongoose.model('Truck', {
    created_by: {
        type: mongoose.Schema.Types.ObjectId,
        required: true
    },
    assigned_to: {
        type: mongoose.Schema.Types.ObjectId,
        // required: true
    },
    type: {
        type: String,
        enum: ['SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT'],
        required: true
    },
    status: {
        type: String,
        enum: ['OL', 'IS'],
        default: 'IS'
    },
    created_date: {
        type: Date,
        default: Date.now()
    }
});

module.exports = { Truck };

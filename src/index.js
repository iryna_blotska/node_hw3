const express = require('express');
const morgan = require('morgan')
const mongoose = require('mongoose');
const app = express();

// const {booksRouter} = require('./controllers/booksController'); 
const {userRouter} = require('./controllers/userController');
const {authRouter} = require('./controllers/authController'); 
const {loadRouter} = require('./controllers/loadController'); 
const {truckRouter} = require('./controllers/truckController'); 
const {authMiddleware} = require('./middlewares/authMiddleware'); 
const {NodeCourseError} = require('./utils/errors'); 

app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use(authMiddleware);
//app.use('/api/books', [authMiddleware], booksRouter);
app.use('/api/users', userRouter);
app.use('/api/loads', loadRouter);
app.use('/api/trucks', truckRouter);

app.use((req, res, next) => {
    res.status(404).json({message: 'Not found'})
});

app.use((err, req, res, next) => {
    if (err instanceof NodeCourseError) {
        return res.status(err.status).json({message: err.message});
    }
    res.status(500).json({message: err.message});
});

const start = async () => {
    try {
        await mongoose.connect('mongodb+srv://testuser99:B6KrpxjrZD3B5AG@cluster0.ogaqw.mongodb.net/hw3?retryWrites=true&w=majority', {
            useNewUrlParser: true, useUnifiedTopology: true
        });
    
        app.listen(8080);
    } catch (err) {
        console.error(`Error on server startup: ${err.message}`);
    }
}

start();
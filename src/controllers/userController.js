const express = require('express');
const router = express.Router();
const {getUserProfile, changeUserPassword, deleteUserProfile} = require('../services/userService');

const {
    asyncWrapper
} = require('../utils/apiUtils');

router.get('/me', asyncWrapper(async (req, res) => {
    const {userId} = req.user;
    const user = await getUserProfile(userId);
    res.json({user});
}));

router.patch('/me/password', asyncWrapper(async (req, res) => {
    const {userId} = req.user;
    const {
        oldPassword,
        newPassword
    } = req.body;
    await changeUserPassword({userId, oldPassword, newPassword});
    res.json({message: 'Password changed successfully'});
}));
router.delete('/me', asyncWrapper(async (req, res) => {
    const {userId} = req.user;
    await deleteUserProfile(userId);
    res.json({message: 'Profile deleted successfully'});
}));

module.exports = {
    userRouter: router
}
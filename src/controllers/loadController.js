const express = require('express');
const router = express.Router();

const {
    addLoadforUser,
    getUsersLoads,
    getLoadByIdForUser,
    postLoadByIdForUser,
    getUserActiveLoad,
    iterateLoadState
} = require('../services/loadService');

const {
    asyncWrapper
} = require('../utils/apiUtils');
const {
    InvalidRequestError
} = require('../utils/errors');
let offset = 0;
const limit = 10;
const truckStatus = ['Arrived to Pick Up', 'En route to delivery', 'Arrived to delivery'];
let truckStatusIndex = 0;

router.post('/', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    await addLoadforUser(userId, req.body);

    res.json({message: "Load created successfully"});
}))

router.get('/', asyncWrapper(async (req, res) => {
    const { userId } = req.user;

    const loads = await getUsersLoads(userId, offset, limit);

    res.json({loads});
}));
router.get('/active', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const loads = await getUserActiveLoad(userId);

    res.json({loads});
}));

router.patch('/active/state', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    await iterateLoadState(userId, truckStatus[truckStatusIndex]);

    res.json({message: `Load state changed to ${truckStatus[truckStatusIndex]}`});
    if(truckStatusIndex < truckStatus.length-1){
        return truckStatusIndex++;
    } else {
        return truckStatusIndex = 0;
    }
}));


router.get('/:id', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;

    const load = await getLoadByIdForUser(id, userId);

    if (!load) {
        throw new InvalidRequestError('No load with such id found!');
    }

    res.json({load});
}));
router.post('/:id/post', asyncWrapper(async (req, res) => {
    
    const { userId } = req.user;
    const { id } = req.params;

    const load = await postLoadByIdForUser(id, userId);

    if (!load) {
        throw new InvalidRequestError('No load with such id found!');
    }

    res.json({
        message: 'Load posted successfully',
        driver_found: true
    });
}));

module.exports = {
    loadRouter: router
}
const express = require('express');
const router = express.Router();

const {
    addTruckforUser,
    getUsersTrucks,
    getTruckByIdForUser,
    updateTruckByIdForUser,
    deleteTruckByIdForUser,
    assignTruckToUser
} = require('../services/truckService');

const {
    asyncWrapper
} = require('../utils/apiUtils');
const {
    InvalidRequestError
} = require('../utils/errors');

router.post('/', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    await addTruckforUser(userId, req.body);

    res.json({message: "Truck created successfully"});
}))

router.get('/', asyncWrapper(async (req, res) => {
    const { userId } = req.user;

    const trucks = await getUsersTrucks(userId);

    res.json({trucks});
}));

router.get('/:id', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;

    const truck = await getTruckByIdForUser(id, userId);

    if (!truck) {
        throw new InvalidRequestError('No truck with such id found!');
    }

    res.json({truck});
}));

router.put('/:id', asyncWrapper(async (req, res) => {
    const { userId } = req.user;
    const { id } = req.params;
    const data = req.body;

    await updateTruckByIdForUser(id, userId, data);

    res.json({message: "Truck details changed successfully"});
}));

router.delete('/:id', asyncWrapper(async (req, res) => {
    const {userId} = req.user;
    const { id } = req.params;
    await deleteTruckByIdForUser(userId, id);
    res.json({message: 'Load deleted successfully'});
}));

router.post('/:id/assign', asyncWrapper(async (req, res) => {
    const {userId} = req.user;
    const { id } = req.params;
    await assignTruckToUser(userId, id);
    res.json({message: 'Truck assigned successfully'});
}))




module.exports = {
    truckRouter: router
}